procgen
=======

Tools for procedural generation of random things! Probably crappy tools! Come look at the code if you're in need of a good laugh!

An example of what `namegen` produces:
* The Ancient Forty-Second Unthinkable Celebrants of the Red Unspeakable Third Rite
* The Blood-Soaked Maidens of the Cold Sword
* The Dark Unknowable Rite of the Holy Hidden Queen
* The Celebrants of the Yellow Defiler
* The Keepers of the Resplendent Iron Door
* The Rite of the Eternal Nine
* The Burning Leering Rite of the Infinite Blood-Soaked Door
* The Scottish Screaming Nameless Eyes of the Leering One
* The Maidens of the Ebon Horror
* The Screaming Servants of the Obliterator
* The Unspeakable Sect of the Scepter
* The Infernal Sect of the Resplendant Realm
* The Celebrants of the Gate
* The Celestial Templars of the Glorious Road
* The Followers of the Faithless Year
* The Thousand-Year Global Church of the Timeless Iron Desecrator
* The Eyes of the Crown
* The Order of the Blood-Soaked Hand
* The All-Knowing Infinite Global Temple of the Hidden Beast
* The Endless Leering Eyes of the Sightless Pyramid
* The Infernal Silver Eldritch Templars of the Divine Defiler
* The Servants of the Spire
* The Followers of the Four
* The Glorious Sect of the Dragon
* The Fatal Nameless Hand of the Shapeless God
* The Infernal Endless Eyes of the Beast
* The Servants of the Forgotten Goat
* The Endless Hidden Ageless Keepers of the Nameless Spire
* The Endless Screaming Lifeless Celebrants of the Shapeless Realm
* The Silver Reviled Ebon Keepers of the Mad God
* The Resplendant Red Rite of the Lucid Mountain
* The Celestial Church of the Iron Years
* The Ageless Servants of the Creeping Hand
* The Soviet Duchy of Socialist New Superior Oklabama
* The Old Soviet Confederacy of Arkantucky
* The Shapeless Celebrants of the Eternal Burning Hand
* The Celebrants of the Sapphire Desecrator
* The Iron Celebrants of the Resolute Eternal Legion
* The Blood-Soaked Perpetual Sanctified Sect of the Spire
