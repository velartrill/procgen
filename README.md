procgen
=======

tools for procedural generation of random things! probably crappy tools! come look at the code if you're in need of a good laugh!

an example of what `namegen` produces:

* The Ancient Forty-Second Unthinkable Celebrants of the Red Unspeakable Third Rite
* The Blood-Soaked Maidens of the Cold Sword
* The Dark Unknowable Rite of the Holy Hidden Queen
* The Celebrants of the Yellow Defiler
* The Keepers of the Resplendent Iron Door
* The Rite of the Eternal Nine
* The Burning Leering Rite of the Infinite Blood-Soaked Door
* The Scottish Screaming Nameless Eyes of the Leering One
* The Maidens of the Ebon Horror
* The Screaming Servants of the Obliterator
* The Unspeakable Sect of the Scepter
* The Infernal Sect of the Resplendant Realm
* The Celebrants of the Gate
* The Celestial Templars of the Glorious Road
* The Followers of the Faithless Year
* The Thousand-Year Global Church of the Timeless Iron Desecrator
* The Eyes of the Crown
* The Order of the Blood-Soaked Hand
* The All-Knowing Infinite Global Temple of the Hidden Beast
* The Endless Leering Eyes of the Sightless Pyramid
* The Infernal Silver Eldritch Templars of the Divine Defiler
* The Servants of the Spire
* The Followers of the Four
* The Glorious Sect of the Dragon
* The Fatal Nameless Hand of the Shapeless God
* The Infernal Endless Eyes of the Beast
* The Servants of the Forgotten Goat
* The Endless Hidden Ageless Keepers of the Nameless Spire
* The Endless Screaming Lifeless Celebrants of the Shapeless Realm
* The Silver Reviled Ebon Keepers of the Mad God
* The Resplendant Red Rite of the Lucid Mountain
* The Celestial Church of the Iron Years
* The Ageless Servants of the Creeping Hand
* The Soviet Duchy of Socialist New Superior Oklabama
* The Old Soviet Confederacy of Arkantucky
* The Shapeless Celebrants of the Eternal Burning Hand
* The Celebrants of the Sapphire Desecrator
* The Iron Celebrants of the Resolute Eternal Legion
* The Blood-Soaked Perpetual Sanctified Sect of the Spire

`badname` generates bad fantasy names based on a consistent lexicon. an example of what `badname` produces:

* Bosivar Samabrak Laktaksat Dopuftam ("Cowstamper Horsegrinder Riverspeaker Pigspoiler")
* Batushpok Obluftir ("Table-grasper Rotdrinker")
* Orbadosk Huruftam ("Gutmangler Facespoiler")
* Wosabrak Kiliftir ("Evilgrinder Screamdrinker")
* Oruksat Dopubkis ("Landspeaker Pigcarrier")
* Raswasvis Frivask Dabavas Swastot ("Skullthief Witchdefiler Bookseeker Mancorruptor")
* Uktaft Bositolt Oblubrak ("Selfpounder Cowcrosser Rotgrinder")
* Hurukdash Kilipash ("Faceburner Screamstriker")
* Swaprash Laktaftir ("Manshredder Riverdrinker")
* Swakdash Bilkaftam Oruktaft ("Manburner Poison-spoiler Landpounder")
* Shospuksat Olbuftir Olbubilk Patabrot ("Beerspeaker Blooddrinker Bloodpoisoner Chairseller")
* Wosavtir Odupash Perebrot Topapash ("Evilburner Meadstriker Dirtseller Scarfstriker")
* Swavas Swalwar Orusvis ("Manseeker Mandeceiver Landthief")
* Wosavtir Topadosk ("Evilburner Scarfmangler")
* Dopuvar Pestaswil ("Pigstamper Frogcleaver")
* Pestavask Oblubrak ("Frogdefiler Rotgrinder")
* Kilitolt Frivask Bosiftir Rasvis ("Screamcrosser Witchdefiler Cowdrinker Offal-thief")
* Patakdash Raswaktor Dabavar ("Chairburner Skullsplitter Bookstamper")
* Tolepash Shirivkaft ("Spellstriker Winepoisoner")
* Patakdash Shepash ("Chairburner Loudstriker")
* Odustot Pestaktor ("Meadcorruptor Frogsplitter")
* Orupris Dababrak Oruksat Toletolt ("Landsailor Bookgrinder Landspeaker Spellcrosser")
* Patakdash Olbuktor Dabavtir Tiftipash ("Chairburner Bloodsplitter Bookburner Foulstriker")