let adjs = [| "queer"; "electric"; "space"; "super"; "discount"; "extreme"; "far-left";
"far-right"; "unreconstructed"; "modern"; "eternal"; "original"; "historical"; "tankie"; "mystical"; "arcane"; "catholic"; "protestant"; "irish"; "dialectical"; |]
let prefixes = [|"neo"; "retro"; "anti"; "counter"; "pre"; "paleo"; "femino"; "anarcho"; "oligo"; "meta"; "diabolo"; "theo"; "petro"; "sado"; "arachno"; "psycho"; "eroto";
"nano"; "mini"; "xeno"; "astro"; "femto"; "electro"; "endo"; "arch"; "macro"; 
"cardio"; "mesmo"; "chrono"; "nycto"; "megalo"; "lesbo"; "masculo"; "ultra"; 
"proto"; "weebo"; "thanato"; "volcano"; "strangulo"; "quasi"; "pseudo"; "procto";
"copro"; "hydro"; "gastro"; "thermo"; "mystico"; "arcano"; "disastro"; "vagino"; |]
let ists  = [| "pluralis"; "feminis"; "activis"; "socialis"; "marxis"; "leninis"; "stalinis"; "fascis"; "sadis"; "stirneris"; "masochis"; "imperialis"; "arcanis"; "syndicalis"; "masturbatis"; "pedanticis"; "posadis"; "hoxhais"; "maois"; "papis"; "catholicis"; "protestantis"; "colonialis"; "francois"; "materialis"; "immaterialis"; "sexualis"; "stallmanis"; "minarchis"; "totalitarianis"; "authoritarianis"; |]

let range min max = (Random.int (max-min)) + min
let chance n = range 0 n = 0
let pick (r: 'a array) : 'a = r.(range 0 (Array.length r));;

let rec accumulate fn times = fn () ^ if times = 0 then "" else " "^(accumulate fn (times-1))
let makeist () = (if chance 2 then (pick prefixes)^"-" else "") ^ (pick ists)^"t"
let ideology () = (if chance 3 then (pick adjs)^" "  else "" ) ^ (accumulate makeist (range 0 2)) ^ " " ^ (if chance 2 then ((if chance 3 then pick prefixes else "") ^"anarchism") else (pick [|"anarcho-";"arachno-"|])^(pick ists)^"m")
let () = Random.self_init (); print_string (ideology ())
	
