let suffixes = [| "tine"; "zone"; "done"; "dol"; "mycin"; "pentin"; "nine"; "cillin"; "mab"; "xone"; "xine"; "zine"; "dine"; "nyl"; "nil"; "codone"; "morphone"; "drene"; "phine"; "thine"; "toxin"; "xin"; "dioxin"; "lene"; "ne"; "pine"; "relin"; "gen"; "phen"; "fen"; "dem"; "lone"; "fen"; "line"; "but"; "te"; "xate"; "tal"; "mate"; "norphine"; |]
let salts = [| "chloride"; "acetate"; "phosphate"; "hydrochloride"; "succinate"; "sulphate"; |]
let prefixes = [| "neo"; "retro"; "nor"; "tri"; "cycli"; "tetra"; "penta"; "methyl"; "nal"; "pro"; "metha"; "fenta"; "meta"; "hydro"; "fluoro"; "thieno"; "mal"; "eroto"; "diablo"; "miso"; "propy"; "ethy"; "buta"; "hexa"; "benzo"; "piperi"; "sado"; "maso"; "philo"; "theo"; "iso"; "cyclo"; "klepto"; "pyro"; "carba"; "ibu"; "masculo"; "phos"; "estro"; "endo"; "poly"; "bi"; "tri"; "cumulo"; "strato"; "dextro"; "sinistro"; "levo"; "dextra"; "sinistra"; "ecto"; "myco"; "canna"; |]
let onsets = [| "f"; "fl"; "fr"; "p"; "pr"; "pl"; "c"; "cr"; "cl"; "s"; "sc"; "scl"; "scr"; "ph"; "sph"; "phr"; "phl"; "ch"; "chr"; "chl"; "z"; "th"; "thr"; "r"; "x"; |]
let vowels = [| "a"; "e"; "i"; "o"; "u"; "y"; |]
let codas = [| "n"; "m"; "r"; "s"; "f"; |]
let pick a = a.(Random.int(Array.length a))
let range min max = min+(Random.int(max-min))

let syl () = match range 0 3 with
	| 0 -> (pick onsets) ^ (pick vowels) ^ (pick codas)
	| 1 | 2 -> (pick onsets) ^ (pick vowels)
	| _ -> (pick vowels) ^ (pick codas)
let rec cat op fn times =
	if times = 0 then "" else (op (fn()) (cat op fn (times-1)))

let drug () =
	let prefct = range 0 3 in
	let sylct = if prefct= 0 then range 1 3 else range 0 3 in
	(cat (^) (fun () -> pick prefixes) prefct) ^
	(cat (^) syl sylct) ^
	(pick suffixes) ^ 
	(if range 0 7 = 0 then " "^(pick salts) else "")
let () = Random.self_init (); print_string (drug ()); print_string "\n";
