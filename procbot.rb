# encoding: UTF-8
require "cinch"
Bots = ["cclg", "sext", "sect", "order", "monster", "pharma", "argue","crime","drug","badbio","bottist","egret_txt", "babdio", "botsoc"]
ShoutingMatch = /\b(MS\.|MS\s|SERGEANT\s|SGT\s|SGT\.)?\s*(SAMANTHA TAGGART|SAMANTHA TAGUE|SAMANTHA|SAM TAGUE|SAM TAGGART|SAMTAGUE|SAMMY|SAM|TAGUE|TAGGART)\b/
SmallMatch = /\b(ms\.|ms\s|sergeant\s|sgt\s|sgt\.)?\s*(samantha taggart|samantha tague|samantha|sam tague|sam taggart|samtague|sam|sammy|tague|taggart|(ね|)サム|(ね|)サムさん|(ね|)サムちゃん|سام|يا سام)\b/i
FormalQs = [
	"How may I be of assistance",
	"Yes",
	"You rang",
	"What can I do for you today",
	"Yes",
	"You rang",
]
def mult(a,b)
	b.map{|bi| a.map{|ai| ai+bi}}.flatten
end
$lastcmd = ""
bot = Cinch::Bot.new do
	configure do |c|
		c.server = "irc.freenode.org"
		c.channels = ["##crimehole"]
		c.nick = "samtague"
		c.user = "samtague"
		c.sasl.username = "samtague"
		c.sasl.password = "bott-ist-tot"
	end

	on :message, /\b(ms\.|ms\s|sergeant\s|sgt\s|sgt\.)?\s*(samantha taggart|samantha tague|samantha|sam tague|sam taggart|samtague|sam|tague|taggart|(ね|)サム|(ね|)サムさん|(ね|)サムちゃん|سام|يا سام)\b/i do |m|
		
		@m=m
		@shouting = false
		def reply(x)
			if @shouting
				@m.reply x.upcase
			else
				@m.reply x
			end
		end
		formality = :friendly
		msg = m.params[1]
		if par = ShoutingMatch.match(msg)
			@shouting = true
			cmd=msg.split(ShoutingMatch,2)
		elsif par = SmallMatch.match(msg)
			@shouting = false
			cmd=msg.split(SmallMatch,2)
		end
		ranks = {
			"velartrill" => ["Admiral", "Admiral Hale"].sample,
			"mtp" => "General",
			"alfiepates" => "General",
			"caurinus" => "Captain",
		}
		lowranks = [ "Corporal", "Private", "Trooper" ]
		user = m.user.nick
		if ranks.include?(user)
			rank=ranks[user]
		else
			rank="Commander"
		end
		cmd.map! {|x| x.strip}
		
		cmd[1].downcase!
		if cmd[1] == "ms" or cmd[1] == "ms."
			formality = :formal
			name = cmd[2].downcase
			action = cmd[3].downcase
		elsif cmd[1] == "sergeant" or cmd[1]=="sgt" or cmd[1]=="sgt."
			formality = :military
			name = cmd[2].downcase
			action = cmd[3]
		else
			formality = :friendly
			name = cmd[1].downcase
			action = cmd[2]
		end
		if action == ""
			action = cmd[0].downcase
			action.sub!(/\W*$/,'')
			greeting = ""
		else
			greeting = cmd[0].downcase
		end

		greetings = ["hey", "hi", "yo", "hello","hey there","hi there","yo there","heya","what up","oh shit","aw yeah","ahlan","hujambo","اهلن", "你好","morning","evening","afternoon","good morning","good evening","good afternoon", "buenos días", "buenos dias", "buenos tardes", "bonjour", "ciao", "bonsoir", "howdy"]
		if greetings.include?(action.sub(/^\W*/,'').sub(/\W*$/,''))
			greeting = action
			action = ""
		end
		action.sub!(/^\W*/,'')
		action.sub!(/^please/,'')
		action.sub!(/please$/,'')
		greeting.sub!(/^\W*/,'')
		greeting.sub!(/\W*$/,'')
		lowrank = false
		if formality == :military
			if action != ""
				if action.sub!(/^sir\b/i,'') or action.sub!(/\bsir$/i,'') 
					rank = lowranks.sample
					lowrank = true
					action.strip!
				end
			end
		end
		nulls = {
			:friendly => [
				"that's my name, don't wear it out",
				"yes?",
				"what's up?",
				greetings.sample,
				greetings.sample+", "+user,
				"what's going on?"
			],
			:formal => FormalQs.map {|x| x+"?"} + FormalQs.map {|x| x+", "+user+"?"},
			:military => lowrank ? [
				"Report, soldier!",
				"Stand down, " + rank + "!",
				"Hold your horses, " + rank + "!",
				rank + " " + user + "! Report!",
				"Report!"
			] : [
				"Sir!",
				"What's the situation, sir?",
				"What's the situation, "+rank+"?",
			]
		}
		affirmatives = {
			:friendly => [
				"sure thing.",
				"no problem.",
				"no worries.",
				"will do.",
				"np!",
				"sure!",
				"okay!"
			],
			:formal => [
				"Of course.",
				"Right away.",
				"At once.",
				"As you wish.",
				"Certainly.",
				"Pleased to assist.",
				"At your command."
			],
			:military => [
				"Sir yes sir!",
				"Ten-four!",
				"Wilco!",
				"Roger that!",
				"Roger that, sir!",
				"Roger!",
				"Wilco, sir!",
				"Wilco, "+rank+"!",
				"Roger, " +rank+"!",
			]
		}
		def scancmd(x,strs)
			strs.each { |str|
				return "" if x==str
				if x.start_with?(str)
					return x[str.length+1,x.length]
				end
			}
			return false
		end
		def perform(x,user)
			y=x.split(" ",2)
			if Bots.include?(y[0].downcase)
				$lastcmd = x
				msg = ""
				bot = "./"+y[0].downcase
				IO.popen(y.length == 2 ? [bot, y[1]] : bot) { |pd|
					msg = pd.read
				}
				return msg
			elsif scancmd(x,["what do you think","what are you thinking about","penny for your thoughts","what's your opinion","opinions"])
				$lastcmd="botsoc"
				return perform("botsoc",user)
			elsif scancmd(x,["again", "make another", "do another", "tell another", "make us another", "encore", "more", "do that again", "once more" "one more time", "do it again", "play it again"])
				if $lastcmd != "" then
					return perform($lastcmd,user)
				else
					return nil
				end
			elsif bot=scancmd(x,
				(mult((mult ["give","tell","make","invent","find","i want","we want","i'd like","we'd like","can i have","can we have","can i get","can we get","create","let's get","let's have","let's hear","lemme have","let me have"], [""," me", " us"]) +
					["make up", "come up with","do","how about","maybe"], [""," a", " an", " a new", " another", " more", " some", " some more"])+
					["more","another","a","a new","an","some"]).reverse)
					# this is the worst thing i have ever done
					# i am so sorry
				case bot
					when "crime", "crimes", "felony", "felonies"
						return perform("crime",user)
					when "word", "term", "weird word", "long word", "jargon", "obscure word",
						"words", "terms", "weird words", "long words", "obscure words"
						return perform("cclg",user)
					when "monster", "beast", "monsters", "beasts"
						return perform("monster",user)
					when "drug", "drugs","street drug","street drugs"
						return perform("drug",user)
					when "pill", "pills", "pharmaceutical", "pharmaceuticals", "legal drug", "legal drugs"
						return perform("pharma",user)
					when "bad bio", "bad bios", "bad twitter bio", "bad twitter bios"
						return perform("badbio",user)
					when "order", "orders", "my orders", "our orders", "our marching orders","plan of action","our plan of action","our strategy","strategy"
						return perform("order",user)
					when "sect", "cult", "sects", "cults"
						return perform("sect",user)
					when "sext"
						return perform("sext",user)
					when "argument", "arguments", "fight", "fights"
						return perform("argue",user)
					when "ideology", "anarchism", "politics", "belief", "creed", "joke"
						return perform("bottist",user)
					when "verse", "ominous statement", "line", "prophecy"
						return perform("egret_txt",user)
					when "analysis", "opinion", "opine"
						return perform("botsoc",user)
				end
			elsif bot=scancmd(x,["fight with","fight"])
				$lastcmd = x
				return perform("argue",user)
			else
				return nil
			end
		end
		negatives = {
			:friendly => [
				"no can do",
				"sorry, not happening",
				"i don't know how",
				"what?",
				"huh?",
				"i'm confused",
				"????",
				"what are you asking me to do",
				"um, no?",
				"um, no??",
				"that's fucked up",
				"isn't that a little problematic",
				"what do you mean",
				"that doesn't make any sense",
				"i don't understand",
				"what are you talking about",
				"how?",
				"what's going on",
				"what's going on?",
				"im scared",
				"i'm scared",
				"calm down " + user,
				"what are you talking about, " + user,
				"i never understand what "+user+" is on about",
				"im dying",
				"help",
				"lol",
				"lmao",
				"what",
				"okay but why",
				"okat but how",
				"who do i need to kill",
				";)",
				"don't make me come over there",
				"oh really",
				"i won't",
				"no"
			],
			:formal => [
				"I'm afraid I can't do that.",
				"I'm afraid that won't be possible.",
				"That won't be possible.",
				"No can do, I'm afraid.",
				"I don't think I can help you with that.",
				"I'm sorry to report that is beyond my capabilities at present.",
				"I don't think I can do that, I'm sorry to say.",
				"Goodness! I would never dream of such a thing.",
				"Certainly not! I have a reputation to think of.",
				"Could you rephrase that?",
				"I'm sorry, I don't think I understand"
			],
			:military => lowrank ? [
				"Not happening, "+rank+"!",
				"Don't even think about it, "+rank+"!",
				"Do I look like I was born yesterday, "+rank+"!",
				"Not happening, "+rank+" "+user+"!",
				"Don't even think about it, "+rank+" "+user+"!",
				"Do I look like I was born yesterday, "+rank+" "+user+"!",
				"Drop and give me twenty!",
				"That's it, you're on KP duty!",
				"No backtalk, maggots!"
			] : [
				"Negative, sir.",
				"Cannot comply, sir.",
				"Not possible, "+rank+".",
				"Negative, sir, we're pinned down!",
				"We don't have the troops for that, "+rank+"!",
				"Sorry, sir, there's nothing I can do."
			]
		}
		whoareyou = {
			:friendly => [
				"i'm sam!",
				"i'm sam tague!",
				"samantha taggart",
				"ash's sister",
				"ash's little sister",
				"ash's younger sister"
			], :formal => [
				"I am Samantha Taggart.",
				"My name is Samantha Taggart.",
				"Taggart. Samantha Taggart.",
				"The name is Samantha Taggart.",
				"The name is Taggart.",
				"Samantha Taggart, esquire."
			], :military => [
				"Sergeant Samantha Taggart reporting for duty, sir!",
				"Sergeant First Class Sam Taggart, sir!",
				"Sergeant Taggart, sir.",
				"Sergeant Taggart, sir. The boys call me Tague.",
				"Sergeant Taggart, sir. But you can call me Sam.",
				"Sergeant Taggart, sir. Call me Sam."
			]
		}
		goodgirl = {
			:friendly => [
				"^_^", ":)",
				"thanks!",
				"you're sweet",
				"just doing my job!",
				"that's really nice of you",
				"aw, do you mean it?",
				"aw, really?",
				"thank you!",
			], :formal => [
				"Of course. I aim to please.",
				"Happy to be of service.",
				"Just doing my job.",
				"It's my pleasure.",
				"I'm just glad I can be of service.",
				"You're too kind.",
				"You're far too kind.",
				"That's very kind of you.",
				"Why, thank you!",
				"It's good to feel appreciated.",
				"It's always good to feel appreciated.",
			], :military => [
				"Yes, sir!",
				"Sir, yes, sir!",
				"Roger that, sir!",
				"Thank you, sir!",
				"You're not so bad yourself, sir.",
				"Thank you, sir. And may I just say - it's been a pleasure serving under you, sir.",
				"Godspeed, sir!"
			]
		}
		dontcare = {
			:friendly => [
				"well you should care",
				"why not",
				"why don't you care",
				"you should care",
				"this matters, " + user,
				"then you don't understand",
				"caring is cool",
				"caring is cool tho"
			], :formal => [
				"May I submit that you should, in fact, care?",
				"I do not believe you appreciate the import of this matter.",
				"This is more important than you realize.",
				"I hope you will reconsider.",
				"Apathy helps no one.",
				"Apathy solves nothing.",
				"Your apathy is shameful."
			], :military => lowrank ? [
				"Didn't your momma ever tell you caring is cool, soldier?",
				"Don't give me that, "+lowranks.sample+"!",
				"Drop and give me twenty, you apathetic worm!",
				"You don't care? I don't care that you don't care! Drop and give me twenty!",
				"No backtalk!"
			] : [
				"With all due respect, sir, caring is cool, sir!",
				rank+", sir, I urgently request that you consider your position, sir!",
				"Sir, with all due respect, I believe you are mistaken as to the importance of the situation, sir.",
				"Sir, this situation is rapidly spiraling out of control.",
				"But "+rank+", this situation is rapidly spiraling out of control."
			]
		}
		thankyou = {
			:friendly => (mult ["np","no worries","sure thing","sure","ofc","of course","glad to help","don't worry about it","just glad to help","no problem","you got it"], ["", "!", "."]),
			:formal => [
				"You're welcome.",
				"You're very welcome.",
				"It was no trouble.",
				"It was no trouble at all.",
				"It was a pleasure.",
				"It was a pleasure, truly.",
				"It was my honor.",
				"I'm pleased to be of service.",
				"Pleased to be of service.",
				"Of course.",
				"Don't mention it.",
				"Oh, don't mention it."
			], :military => [
				"No thanks necessary, sir!",
				"Just doing my job, sir.",
				"It's what they pay me for, sir.",
				"Sir, yes, sir!"
			]
		}
		insults = {
				:friendly => [
					"i don't like you either, pal",
					"fuck you", "fuck you too",
					"get fucked", "fuck off", "suck this",
					"lick my clit", "lick my tits", "suck my clit",
					"suck my tits", "i'm gonna fuck you up",
					"big talk coming from you, "+user,
					"you're on my list now, "+user,
					"go fuck yourself",
					"i'll remember this",
					"do you kiss your momma with that mouth?",
					"die in a fire","get bent","go to hell",
					"wow rude", "chill out", "take a chill pill",
					"you need to calm down", "lmao",
					"get bent", "does it look like i give a fuck what you think about me",
					"you're problematic", "hey everyone "+user+" sucks donkey balls",
					"hey everyone "+user+" jacks off to schwarzenegger movies",
					"hey everyone "+user+" killed JFK",
					"hey everyone "+user+"'s actually just hundreds of lizards dressed up like a person",
					"hey everyone "+user+" uses diphenhydramine recreationally",
					"meanie", "why are you like this", "take that back",
					"you take that back", "you take that back right now",
					"you hurt my feelings", "don't hurt my feelings like that",
					"i thought we were friends!!", "you're a horrible person, "+user,
					"please stop being mean to me", "why are you so cruel"
				], :formal => [
					"How rude!", "Goodness!", "Goodness, how rude!", "Goodness gracious!",
					"You would do well to treat your betters with more respect than that.",
					"You've made an enemy today, "+user+".", "I won't tolerate such language!",
					"I won't tolerate such disrespect!",
					"I'm sorry, I was unaware my duties would include babysitting today!",
					"Please show yourself out.", "Your services will no longer be required.",
					"This is hardly the place for such infantile behavior.",
					"Why must you be like this?", "Unacceptable!",
					"Goodness gracious, why must you be like this?"
				], :military => [
					"What the hell did you just say to me, maggot?",
					"Insubordination!",
					"I'll have you flogged for this!",
					"You got an awful big mouth on you, "+lowranks.sample+". Let's see you wash the floor with it!",
					"Insubordination will not be tolerated!",
					"You'll hang for this!",
					"Out of line!",
					"You're out of line!",
					"You're out of line, soldier!",
					"Out of line, "+lowranks.sample+" "+user+"!",
					"You're out of line, "+lowranks.sample+" "+user+"!"
				]
			}
			if param=scancmd(action,["meet","this is","these are",
						"let me introduce you to","let me introduce",
						"allow me to introduce you to",
						"allow me to introduce",
						"say hi to","say hello to",
						"introduce yourself to"])
				param.sub!(/\W*$/,'')
				reply [
					"pleased","nice","good",
					"i'm so happy",
					"i'm so glad",
					"it's so nice",
					"it's so good"
				].sample+" to "+[
					"meet you",
					"get to meet you",
					"make your acquaintance"
				].sample+", "+param+(rand(2)==0 ? ". "+user+" has told me so much about you" : "")
			elsif param=scancmd(action,["good night","night","gnight","g'night","i'm going to bed","going to bed","im going to bed", "bedtime", "time for bed", "it's my bedtime","its my bedtime","nighty-night"])
				reply mult(["good night","night","sleep well","sleep tight","i wish you an enjoyable cycle of nocturnal hallucinations", "commencement of dormancy phase acknowledged"], ["", ", "+user, ", pesky flesh-creature", ", human", ", mortal"]).sample
			elsif param=scancmd(action,["i don't care","it doesn't matter","don't care","idc","nevermind","never mind","nvm","i don't give a shit","i don't give a fuck", "does it look like i care"])
				reply dontcare[formality].sample
			elsif param=scancmd(action,["fuck you","fuck off","go fuck yourself","get fucked","fu","get bent","diaf","die in a fire","screw you","you cunt","you bitch","you piece of shit","you scumbag","you sleazeball","you jackass","you asshole","you cockroach"])
				reply insults[formality].sample
			elsif param=scancmd(action,["thanks","thank you","i appreciate it", "ty", "thx", "obliged", "much obliged","appreciated","danke","gracias","merci","merci beaucoup","shukran","shukran jazeelan","asante","asante sana","arigatou","doumo arigatou", "ありがとう", "どうもありがとう", "どうもありがとうございます","شكران", "شكران جزيلاا"])
				reply thankyou[formality].sample
			elsif param=scancmd(action,["well done","good work","excellent work","good girl","nicely done","nice job","good job","perfect","you did well","excellent job","you did great","you did perfect","you did perfectly","wonderful","you did good","gj"])
				reply goodgirl[formality].sample
			elsif param=scancmd(action,["who are you","who do you think you are","who even are you","tell me who you are","tell us who you are","introduce yourself"])
				reply whoareyou[formality].sample
		elsif greeting==""
			if action==""
				reply nulls[formality].sample
			else
				result=perform(action,user)
				r=""
				if result != nil
					result.chomp!
					if formality != :friendly
						r=affirmatives[formality].sample+" "
						result[0] = result[0].upcase
						result += "." if not result=~/\W$/
					end
					reply r+result.chomp
				else
					reply negatives[formality].sample
				end
			end
		else
			if action==""
				if formality == :friendly
					reply (nulls[:friendly] + [
						greeting+" yourself"
					]).sample
				elsif formality == :formal
						r = greeting
						r[0]=r[0].upcase
						reply r +", " + user + "."
				elsif formality == :military
					if lowrank
						reply [
							"You're out of line, "+rank+"!",
							"Cut the chatter, soldier!",
							"Stow the chit-chat, "+rank+"!",
						].sample
					else
						reply rank+", sir!"
					end
				end
			else
				if formality == :military and lowrank
					reply negatives[formality].sample
					return
				end
				result=perform(action, user)
				r=""
				if result != nil
					result[0] = result[0].upcase if formality != :friendly
					r=affirmatives[formality].sample
					result.chomp!
					result += "." if not result=~/\W$/
					reply r+" "+result
				else
					reply negatives[formality].sample
				end
			end
		end
	end
	on :message, /daddy/i do |m|
		m.reply "NO DDLG"
	end
	on :message, /monad/i do |m|
		m.reply "NO HASKELL"
	end
	on :message, /satan/i do |m|
		lucy = ["hail satan", "ave satanas", "all glory to the prince of darkness"]
		lucy += lucy.map { |x| x.upcase }
		m.reply(mult(lucy, ["",".","!","!!","!!1"]).sample)
	end
	
	on :message, /there/i do |m|
		m.reply ["*they're","*their"].sample if rand(50)==0
	end
	on :message, /your/i do |m|
		m.reply ["*you're","*yore"].sample if rand(50)==0
	end
	on :online, "velartrill" do |m|
		m.reply "ALL RISE"
	end
end

bot.start
