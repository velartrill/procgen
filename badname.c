// badname.c
// generates awful fantasy names based on a consistent
// lexicon.
// copyright 2014 alexis hale. released under GPLv2.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define to(ty, ctr, end) for(ty ctr = 0; ctr<end; ++ctr)

typedef unsigned char u8;

struct morpheme {
	const char* nat;
	const char* translation;
};

struct morpheme nouns[] = {
	{"Ilu", "Stone"},     {"Siri", "Water"},  {"Raswa", "Skull"},
	{"Nishra", "Spleen"}, {"Olbu", "Blood"},  {"Oblu", "Rot"},
	{"Bolu", "Weed"},     {"Huru", "Face"},   {"Pere", "Dirt"},
	{"Sarwa", "Filth"},   {"Oru", "Land"},    {"Barna", "Stomach-"},
	{"Orba", "Gut"},      {"Pata", "Chair"},  {"U", "Self"},
	{"Ra", "Offal-"},     {"Batu", "Table-"}, {"Topa", "Scarf"},
	{"Bosi", "Cow"},      {"Sama", "Horse"},  {"Dopu", "Pig"},
	{"Lakta", "River"},   {"Lakta", "Mine"},  {"Fri", "Witch"},
	{"Tole", "Spell"},    {"Daba", "Book"},   {"Ipa", "Ale"},
	{"Odu", "Mead"},      {"Shiri", "Wine"},  {"Shospu", "Beer"},
	{"Kili", "Scream"},   {"She", "Loud"},    {"Ro", "Quick"},
	{"Tifti", "Foul"},    {"Wosa", "Evil"},   {"Bilka", "Poison-"},
	{"Swa", "Man"},       {"Pesta", "Frog"},
}, acts[] = {
	{"ktor", "splitter"}, {"swil", "cleaver"},   {"tfas", "burster"}, 
	{"dosk", "mangler"},  {"stot", "corruptor"}, {"brak", "grinder"},
	{"vas", "seeker"},    {"ftir", "drinker"},   {"var", "stamper"},
	{"shirk", "smeller"}, {"svis", "thief"},     {"brot", "seller"},
	{"tolt", "crosser"},  {"kdash", "burner"},   {"pris", "sailor"},
	{"ksat", "speaker"},  {"bkis", "carrier"},   {"shvor", "slayer"},
	{"klis", "avenger"},  {"ftam", "spoiler"},   {"lwar", "deceiver"},
	{"shpok", "grasper"}, {"pash", "striker"},   {"pris", "render"},
	{"prash", "shredder"},{"vask", "defiler"},	 {"dar", "dweller"},
	{"vtir", "burner"},   {"bilk", "poisoner"},  {"vkaft", "poisoner"},
	{"ktaft", "pounder"}, {"rpift", "strangler"},
};
typedef struct name_t {
	struct morpheme* noun,* act;
} name_t;

void prt(const char* c) {
	do putchar(*c); while (*(++c)!=0);
}

void pname(name_t* names, u8 len) {
	u8 ct = 0;
	name_t* n = names;
	prt("\e[1m");
	do { 
		prt(n->noun->nat);
		prt(n++->act->nat);
		putchar(' ');
	} while (++ct < len);
	prt("\e[0m(\"\e[4m");
	ct = 0;
	do { 
		prt(names->noun->translation);
		prt(names++->act->translation);
		if (ct != len-1) putchar(' ');
	} while (++ct < len);
	prt("\e[0m\")");
}

typedef struct entity_t {
	name_t* names; u8 namec;
} entity_t;

void make_entity(entity_t* e) {
	e -> namec=rand()%3 + 2;
	e -> names=malloc(sizeof(name_t)*e->namec);
	to (u8, i, e -> namec) {
		e -> names[i].noun =
			&nouns[rand() % sizeof(nouns) / sizeof(struct morpheme)];
		e -> names[i].act =
			&acts[rand() % sizeof(acts) / sizeof(struct morpheme)];
	}
}

int main() {
	srand(time(NULL));
	const u8 entities = 20;
	entity_t* es = malloc(sizeof(entity_t)*entities);
	for (u8 e = 0; e<entities; e++)
		make_entity(&es[e]),
		pname(es[e].names, es[e].namec), putchar('\n');
	return 0;
}